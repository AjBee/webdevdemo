// apisv2/serverv2.js
var express = require('express');
var cors = require('cors');
var app = express();
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors({ origin: true }));
app.use(cors());
const PORT = process.env.PORT || 3000;

app.get("/", (req, res) => {
  res.send("<h2>It's Working!</h2>");
});

// *** Remove ***
//app.use("/api/v2", v2Router);

const v2ProductRouter = require("./v2/routes/productRoutes");
app.use("/api/v2/products", v2ProductRouter);

app.listen(PORT, () => {
  console.log(`API is listening on port ${PORT}`);
  console.log(`call http://localhost:${PORT}/api/v2/products`)
});