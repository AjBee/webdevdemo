// In apisv2/v2/controllers/productController.js
// *** ADD ***
const ProductService = require("../services/productServices");

const getAllProducts = async (req, res) => {
  const allProducts =  await ProductService.getAllProducts();
  res.send(allProducts);
};

const getOneProduct = async (req, res) => {
  console.log(req.params.id)
  const Product = await ProductService.getOneProduct(req.params.id);
  res.send(Product);
};

const createNewProduct = async (req, res) => {
  const createdProduct = await ProductService.createNewProduct(req.body);
  res.send(createdProduct);
};

const updateOneProduct = async (req, res) => {
  const updatedProduct = await ProductService.updateOneProduct(req.params.id, req.body);
  res.send(updatedProduct);
};

const deleteOneProduct = async (req, res) => {
  await ProductService.deleteOneProduct(req.params.id);
  res.send("Delete an existing Product id:" + req.params.id);
};

module.exports = {
  getAllProducts,
  getOneProduct,
  createNewProduct,
  updateOneProduct,
  deleteOneProduct,
};