// In apisv2/v2/routes/productRoutes.js
const express = require("express");
const ProductController = require("../controllers/productControllers");

const router = express.Router();

router.get("/", ProductController.getAllProducts);

router.get("/:id", ProductController.getOneProduct);

router.post("/", ProductController.createNewProduct);

router.put("/:id", ProductController.updateOneProduct);

router.delete("/:id", ProductController.deleteOneProduct);

module.exports = router;