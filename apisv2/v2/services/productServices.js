// var mysql = require('mysql');
const mysql = require(`mysql-await`);
//create database connection
const conn = mysql.createConnection({
    host: 'se.mfu.ac.th',
    user: 'wd22lab',
    password: 'wd22lab@2022',
    database: 'wd22lab_db'
});

//connect to database
conn.connect((err) => {
    console.log(err)
    if (err) throw err;
    console.log('Mysql Connected...');
});

// In src/services/ProductService.js
const getAllProducts = async () => {
    try {
        let sql = "SELECT * FROM products";
        let results = await conn.awaitQuery(sql);
        return JSON.stringify({ "status": 200, "error": null, "response": results });
    } catch (err) {
        return JSON.stringify({ "status": 500, "error": err, "response": results });
    }
};

const getOneProduct = async (id) => {
    try {
        let sql = "SELECT * FROM products WHERE id=?";
        let results = await conn.awaitQuery(sql, id)
        return JSON.stringify({ "status": 200, "error": null, "response": results });
    } catch (err) {
        return JSON.stringify({ "status": 500, "error": err, "response": results });
    }
};

const createNewProduct = async (data) => {
    try {
        let sql = "INSERT INTO products SET ?";
        let results = await conn.awaitQuery(sql, data);
        return JSON.stringify({ "status": 200, "error": null, "response": results });
    } catch (err) {
        return JSON.stringify({ "status": 500, "error": err, "response": results });
    }
};

const updateOneProduct = async (id, data) => {
    try {
        let sql = "UPDATE products SET name=?, detail=?, qty=?,price=? WHERE id=?";
        let results = conn.awaitQuery(sql, [data.name, data.detail, data.qty, data.price, id]);
        return JSON.stringify({ "status": 200, "error": null, "response": results });
    } catch (err) {
        return JSON.stringify({ "status": 500, "error": err, "response": results });
    }
};

const deleteOneProduct = async (id) => {
    try {
        let sql = "DELETE FROM products WHERE id=?";
        let results = await conn.awaitQuery(sql, id);
        return JSON.stringify({ "status": 200, "error": null, "response": results });
    } catch (err) {
        return JSON.stringify({ "status": 500, "error": err, "response": results });
    }
};

module.exports = {
    getAllProducts,
    getOneProduct,
    createNewProduct,
    updateOneProduct,
    deleteOneProduct,
};