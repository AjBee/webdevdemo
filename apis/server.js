var express = require('express');
var cors = require('cors');
var app = express();
var mysql = require('mysql');

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors({ origin: true }));
app.use(cors());

//create database connection
const conn = mysql.createConnection({
    host: 'se.mfu.ac.th',
    user: 'wd22lab',
    password: 'wd22lab@2022',
    database: 'wd22lab_db'
});

//connect to database
conn.connect((err) => {
    if (err) throw err;
    console.log('Mysql Connected...');
});


//Static folder
app.use('/', express.static('../dist'));

//Get all products
app.get('/api/v1/products', (req, res) => {
    let sql = "SELECT * FROM products";
    let query = conn.query(sql, (err, results) => {
        if (err) throw err;
        res.send(JSON.stringify({ "status": 200, "error": null, "response": results }));
    });
});

//Get one products
app.get('/api/v1/products/:id', (req, res) => {
    const id = req.params.id; //product id
    let sql = "SELECT * FROM products WHERE id=?";
    let query = conn.query(sql, id, (err, results) => {
        if (err) throw err;
        res.send(JSON.stringify({ "status": 200, "error": null, "response": results[0] }));
    });
});

//add new product
app.post('/api/v1/products', (req, res) => {
    let data = req.body; //{name:xxx, qty:xxx, price:xxx}
    let sql = "INSERT INTO products SET ?";
    let query = conn.query(sql, data, (err, results) => {
        if (err) throw err;
        res.send(JSON.stringify({ "status": 200, "error": null, "response": results }));
    });
});

//update product
app.put('/api/v1/products/:id', (req, res) => {
    const id = req.params.id; //product id
    const pd = req.body;
    console.log(pd)

    let sql = "UPDATE products SET name=?, detail=?, qty=?,price=? WHERE id=?";
    let query = conn.query(sql, [pd.name, pd.detail, pd.qty, pd.price, id], (err, results) => {
        if (err) throw err;
        res.send(JSON.stringify({ "status": 200, "error": null, "response": results }));
    });
});

//Delete product
app.delete('/api/v1/products/:id', (req, res) => {
    const id = req.params.id;
    let sql = "DELETE FROM products WHEREid=?";
    let query = conn.query(sql, id, (err, results) => {
        if (err) throw err;
        res.send(JSON.stringify({ "status": 200, "error": null, "response": results }));
    });
});


//Server listening
app.listen(3000, () => {
    console.log('Server started on port 3000...');
});
