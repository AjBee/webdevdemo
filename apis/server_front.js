var express = require('express');

var app = express();


app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors({ origin: true }));
app.use(cors());




//Static folder
app.use('/', express.static('./dist'));



//Server listening
app.listen(8233, () => {
    console.log('Server started on port 8233...');
});
