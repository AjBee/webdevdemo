# Pokedex Lab

Create Pokedex from JSON data
```
https://raw.githubusercontent.com/joseluisq/pokemons/master/pokemons.json
```

## Load JSON Pokedex Data
**Java Script**
```javascript
<script>
const URL = "./data/pokedex.json";
  export default {
    data: () => ({
      pokedex: [],
      search: "",
      filterDex: [],
    }),
    created() {
      this.initiaize();
    },
    methods: {
      async initiaize() {
        try {
          const res = await this.$axios.get(URL);
          this.pokedex = res.data.results;
          this.filterDex = this.pokedex;
        } catch (err) {
          console.log(err);
        }
      },
    }
  }
</script>
```

## Create Pokedex card from JSON
**HTML**
```html
<template>
    <v-layout wrap>
      <v-row>
      <v-col v-for="(pk, idx) in this.filterDex" :key="idx">    
        <v-card>
          <v-img height="160px" style="padding: 20px" :src="pk.sprites.large">
          </v-img>
          <v-card-title>{{ pk.name }}</v-card-title>
          <v-card-subtitle class="pb-0"> {{pk.type}}</v-card-subtitle>
          <v-card-actions>
            <v-spacer></v-spacer>
            <v-btn color="orange" text> Add Favorite </v-btn>
          </v-card-actions>
        </v-card>
    </v-col>
    </v-row>
    </v-layout>
  </template>
```

## Update Responsive
**HTML**
```html
<v-col cols="12"  lg="2" md="3" sm="6" xs="12" 
             v-for="(pk, idx) in this.filterDex" :key="idx">
```

## Add Search Bar and Search feature
**HTML inside**
```html
 <v-toolbar>
  
      Pokedex
      <v-spacer></v-spacer>
      <v-text-field
        v-model="search"
        append-icon="mdi-magnify"
        label="Search"
        single-line
        hide-details
      ></v-text-field>
  
 </v-toolbar>
```
**Java Script**
```javascript
    export default {
    data: () => ({
      pokedex: [],
      search: "", // link to search bar
      filterDex: [],
    }),
   ...
    watch:{
        search(val){
            this.filterDex = this.pokedex.filter((pk) =>{
                return pk.name.toLowerCase().indexOf(val.toLowerCase())!=-1;
            })
        }
    }
  };
```