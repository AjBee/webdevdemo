var express = require('express');
var cors = require('cors');
var app = express();
var mysql = require('mysql');

const jwt = require("jsonwebtoken");
const JWT_SECRET = "goK!pusp6ThEdURUtRenOwUhAsWUCLheBazl!uJLPlS8EbreWLdrupIwabRAsiBu";


app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors({ origin: true }));
app.use(cors());

function authenticateToken(req, res, next) {
    const authHeader = req.headers['authorization']
    const token = authHeader && authHeader.split(' ')[1]
    console.log(token)
    if (token == null) return res.sendStatus(401)
    
    jwt.verify(token, JWT_SECRET, (err, user) => {
      console.log(err)
  
      if (err) return res.sendStatus(403)
  
      req.user = user
        console.log(user)
      next()
    })
  }


app.post("/login", (req, res) => {
    const { username, password } = req.body;
    console.log(`${username} is trying to login ..`);
  
    if (username === "admin" && password === "12345") {
      return res.json({
        token: jwt.sign({ user: "admin" }, JWT_SECRET),
      });
    }
  
    return res
      .status(401)
      .json({ message: "The username and password your provided are invalid" });
  });

// curl -X POST http://localhost:3001/login --header "Content-Type: application/json" --data '{ "username": "admin", "password": "12345" }'
// curl -i localhost:3001/super-secure-resource --Header 'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjoiYWRtaW4iLCJpYXQiOjE2NjQxNTA4NjJ9.f57fqLj65kg3Cm_0UYvMMCEuIOPbl3AJOVNct2aSlqg'
app.get("/super-secure-resource", authenticateToken,(req, res) => {
    console.log(req.user)
    res.status(200).json({message:"Your are authen user..."})
});

// First version
// app.get("/super-secure-resource",(req, res) => {
//         res
//       .status(401)
//       .json({ message: "You need to be logged in to access this resource" });
// });
  
  app.listen(3001, () => {
    console.log("API running on localhost:3001");
  });