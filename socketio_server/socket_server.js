const express = require('express')
const app = new express()

const http = require('http').Server(app)

const io = require('socket.io')(http)

io.on('connection', function (socket) {
    // connected io success
    console.log('connected : ID =>', socket.id)
  
    socket.on('sendMessage', (msg) => {
        console.log("Got Msg:" + msg)
      io.emit('replyMessage', msg)
    })
  })

//Static folder
app.use('/', express.static('../dist'));

http.listen(3000);

